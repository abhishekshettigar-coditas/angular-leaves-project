import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Ileave } from '../common/type';

@Component({
  selector: 'app-leave-list',
  templateUrl: './leave-list.component.html',
  styleUrls: ['./leave-list.component.scss'],
})
export class LeaveListComponent implements OnInit {
  @Input('leaves') leaves: Ileave[] = [];
  @Input('title') title: string = '';
  @Input('leaveType') leaveType: string = '';
  @Output() selectLeave = new EventEmitter();

  leaveTypes: string[] = ['All', 'Floater', 'Planned'];
  convertLeave: string[] = [];

  constructor() {}

  ngOnInit(): void {
    for (let leavetype of this.leaveTypes) {
      if (leavetype !== this.leaveType) this.convertLeave.push(leavetype);
    }
  }

  onShift(leave: Ileave, convertLeave: string) {
    this.selectLeave.emit([leave, convertLeave]);
  }
}
