import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[tooltip]',
})
export class ToolTipDirective {
  @Input() tooltip = '';
  @Input() delay? = 190;

  popup!: HTMLDivElement;

  constructor(private el: ElementRef) {}

  @HostListener('mouseenter') onMouseEnter() {
    this.createTooltipPopup();
  }

  @HostListener('mouseleave') onMouseLeave() {
    if (this.popup) {
      this.popup.remove();
    }
  }

  private createTooltipPopup() {
    let popup = document.createElement('div');
    popup.innerHTML = this.tooltip;
    this.el.nativeElement.appendChild(popup);
    popup.style.position = 'absolute';
    this.popup = popup;
  }
}
