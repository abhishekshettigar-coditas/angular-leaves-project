import { Component } from '@angular/core';
import { Ileave } from './common/type';
import { HttpService } from './services/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'leave-management';
  leavesList: Ileave[] = [];
  leaves: Ileave[] = [];
  floaterLeavesList: Ileave[] = [];
  PlannedLeavesList: Ileave[] = [];

  constructor(private data: HttpService) {}

  ngOnInit(): void {
    this.data.displayLeaves().subscribe((result) => {
      this.leavesList = result;
    });
  }

  get allLeaves() {
    return this.leaves = this.leavesList.filter((leave) => leave.leaveType === 'All');
    
  }

  get floaterLeaves() {
    this.floaterLeavesList = this.leavesList.filter(
      (leave) => leave.leaveType === 'Floater'
    );
    return this.floaterLeavesList;
  }

  get plannedLeaves() {
    this.PlannedLeavesList = this.leavesList.filter(
      (leave) => leave.leaveType === 'Planned'
    );
    return this.PlannedLeavesList;
  }

  toggleLeave(data: any) {
    // data.leaveType = data.changeLeave;
    data[0].leaveType = data[1];
  }
}
