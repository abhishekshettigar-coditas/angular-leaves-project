import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Ileave } from '../common/type';

@Injectable({
  providedIn: 'root',
})
export class MockApiService {
  constructor() {}
  leaves: Ileave[] = [
    { id: 1, leaveName: 'Republic Day', leaveType: 'All', leaveDate: '26 Jan' },
    {
      id: 2,
      leaveName: 'Independence Day',
      leaveType: 'Planned',
      leaveDate: '15 Aug',
    },
    {
      id: 3,
      leaveName: 'Gandhi Jayanti',
      leaveType: 'All',
      leaveDate: '02 Oct',
    },
    {
      id: 4,
      leaveName: 'Dassehra',
      leaveType: 'Floater',
      leaveDate: '30 Sep',
    },
    {
      id: 5,
      leaveName: 'Eid Milad',
      leaveType: 'All',
      leaveDate: '19 July',
    },
    {
      id: 6,
      leaveName: 'Diwali',
      leaveType: 'Floater',
      leaveDate: '20 Oct',
    },
    {
      id: 6,
      leaveName: 'Christmas',
      leaveType: 'All',
      leaveDate: '25 Dec',
    },
  ];
  getLeaves() {
    return of(this.leaves);
  }
}
