import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LeaveListComponent } from './leave-list/leave-list.component';
import { ButtonsComponent } from './leave-list/buttons/buttons.component';
import { ToolTipDirective } from './directive/tool-tip.directive';

@NgModule({
  declarations: [
    AppComponent,
    LeaveListComponent,
    ButtonsComponent,
    ToolTipDirective,
  ],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
