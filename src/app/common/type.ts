export interface Ileave {
  id?: number;
  leaveName: string;
  leaveType: string;
  leaveDate: string;
}
