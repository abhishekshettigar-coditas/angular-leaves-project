import { Injectable } from '@angular/core';
import { MockApiService } from '../mock-api/mock-api.service';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private http: MockApiService) {}

  displayLeaves() {
    return this.http.getLeaves();
  }
}
